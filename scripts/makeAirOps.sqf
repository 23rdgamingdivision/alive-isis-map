//////////////////////////////////////
//----ALiVE NATOFOR Random Tasks----//
//---By Valixx, Kylania & M4RT14L---//
//---------------v1.4---------------//
//////////////////////////////////////

_missionType = [_this, 0, ""] call BIS_fnc_param;

sleep 180;

_markerArray = ["aosite","aosite_1","aosite_2","aosite_3","aosite_4","aosite_5","aosite_6","aosite_7","aosite_8","aosite_9","aosite_10"];
_rnd 	= floor (random (count(_markerArray)));
_mrkSpawnSite = getMarkerPos (_markerArray select _rnd);

sleep 60;
															   
fn_spawnArtyMission = {

	hint "UPDATED AIR OPS";
	//creating the marker 

	_markerAO = createMarker ["mob_arty", _mrkSpawnSite];
	_markerAO setMarkerType "mil_join";
	_markerAO setMarkerColor "ColorGreen";
	_markerAO setMarkerText "AIR OP";
	_markerAO setMarkerSize [1,1];
	
	_null = [west, "mob_arty", ["must destroy two opfor MAT", "Destroy Artillery", "Destroy Artillery"], getMarkerPos "mob_arty", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_arty", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerAO, 0, 50, 10, 0, 10, 0] call BIS_fnc_findSafePos;
	
	arty1 = createVehicle ["O_MBT_02_arty_F", _newPos, [], 0, "NONE"];
	arty1 setFuel 0;
	arty1 setVehicleAmmo 1;
	
	arty2 = createVehicle ["O_MBT_02_arty_F", _newPos, [], 0, "NONE"];
	arty2 setFuel 0;
	arty2 setVehicleAmmo 1;
	
	camonet1 = createVehicle ["CamoNet_OPFOR_big_F", getPos _arty1, [], 0, "CAN_COLLIDE"];
	camonet2 = createVehicle ["CamoNet_OPFOR_big_F", getPos _arty2, [], 0, "CAN_COLLIDE"];
	
	armor = createGroup EAST;
	crew1 = armor createUnit ["O_crew_F", [0,0,1], [], 0, "CAN_COLLIDE"];
	crew1 moveInGunner arty1;
	crew2 = armor createUnit ["O_crew_F", [0,0,1], [], 0, "CAN_COLLIDE"];
	crew2 moveInGunner arty2;
	
	grp1A = [getMarkerPos _markerAO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp1A,getpos arty1, 300] call BIS_fnc_taskPatrol;
	
	helos1 = createGroup EAST;
	[_newPos, 10, "O_Heli_Attack_02_F", helos1] call BIS_fnc_spawnvehicle;
	nul = [helos1,getMarkerPos _markerAO, 800] call BIS_fnc_taskPatrol;
	sleep 10;
	[_newPos, 10, "O_Heli_Attack_02_F", helos1] call BIS_fnc_spawnvehicle;

	waitUntil { !alive arty1 && !alive arty2 };
	
	_null = ["mob_arty", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerAO;
	
	{deleteVehicle _x} forEach units grp1A;
	{deleteVehicle _x} forEach units helos1;
	deleteGroup grp1A;
	deleteGroup helos1;
	deleteGroup armor;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_arty"] call LARs_fnc_removeTask;

	sleep 300;
};

fn_spawnRadarMission = {

	hint "UPDATED AIR OPS";
	//creating the marker 

	_markerAO = createMarker ["mob_radar", _mrkSpawnSite];
	_markerAO setMarkerType "mil_join";
	_markerAO setMarkerColor "ColorGreen";
	_markerAO setMarkerText "AIR OP";
	_markerAO setMarkerSize [1,1];
	
	_null = [west, "mob_radar", ["Eliminate the AA Radar.", "Eliminate AA Radar", "Eliminate AA Radar"], getMarkerPos "mob_radar", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_radar", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerAO, 0, 100, 10, 0, 0, 0] call BIS_fnc_findSafePos;
	
	radar = createVehicle ["Land_Radar_Small_F",[(getMarkerpos _markerAO select 0) + 3, getMarkerpos _markerAO select 1,0],[], 0, "NONE"];
	
	grp1A = [getMarkerPos _markerAO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp1A,getpos _radar, 200] call BIS_fnc_taskPatrol;
	
	AA1 = createGroup EAST;
	[_newPos, 10, "O_APC_Tracked_02_AA_F", AA1] call BIS_fnc_spawnvehicle;
	nul = [AA1,getMarkerPos _markerAO, 100] call BIS_fnc_taskPatrol;
	sleep 15;
	[_newPos, 10, "O_APC_Tracked_02_AA_F", AA1] call BIS_fnc_spawnvehicle;
	sleep 15;
	[_newPos, 10, "O_APC_Tracked_02_AA_F", AA1] call BIS_fnc_spawnvehicle;

	waitUntil { !alive radar };
	
	_null = ["mob_radar", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerAO;
	
	{deleteVehicle _x} forEach units grp1A;
	{deleteVehicle _x} forEach units AA1;
	deleteGroup grp1A;
	deleteGroup AA1;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_radar"] call LARs_fnc_removeTask;

	sleep 300;
};

fn_spawnDepotMission = {

	hint "UPDATED AIR OPS";
	//creating the marker 

	_markerAO = createMarker ["mob_depot", _mrkSpawnSite];
	_markerAO setMarkerType "mil_join";
	_markerAO setMarkerColor "ColorGreen";
	_markerAO setMarkerText "AIR OP";
	_markerAO setMarkerSize [1,1];
	
	_null = [west, "mob_depot", ["Eliminate the OPFOR command post.", "Destroy HQ", "Destroy HQ"], getMarkerPos "mob_depot", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_depot", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerAO, 0, 50, 10, 0, 0, 0] call BIS_fnc_findSafePos;
	
	shed = createVehicle ["Land_Cargo_HQ_V2_F",[(_newPos select 0), _newPos select 1,0],[], 0, "NONE"];
	trck = createVehicle ["O_Truck_03_device_F",[(_newPos select 0) +3, _newPos select 1,0],[], 0, "NONE"];
	
	grp1A = [getMarkerPos _markerAO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp1A,getpos _shed, 400] call BIS_fnc_taskPatrol;
	
	planes1 = createGroup EAST;
	[_newPos, 10, "O_Plane_CAS_02_F", planes1] call BIS_fnc_spawnvehicle;
	nul = [planes1,getMarkerPos _markerAO, 1500] call BIS_fnc_taskPatrol;
	sleep 10;
	[_newPos, 10, "O_Plane_CAS_02_F", planes1] call BIS_fnc_spawnvehicle;

	waitUntil { !alive shed };
	
	_null = ["mob_depot", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerAO;
	
	{deleteVehicle _x} forEach units grp1A;
	{deleteVehicle _x} forEach units planes1;
	deleteGroup planes1;
	deleteGroup grp1A;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_depot"] call LARs_fnc_removeTask;

	sleep 300;
};

fn_spawnConvoyMission = {

	hint "UPDATED AIR OPS";
	//creating the marker 

	_markerAO = createMarker ["mob_convoy", _mrkSpawnSite];
	_markerAO setMarkerType "mil_join";
	_markerAO setMarkerColor "ColorGreen";
	_markerAO setMarkerText "AIR OP";
	_markerAO setMarkerSize [1,1];
	
	_null = [west, "mob_convoy", ["Attack and destroy the supply convoy", "Eliminate Convoy", "Eliminate Convoy"], getMarkerPos "mob_convoy", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_convoy", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerAO, 0, 50, 10, 0, 0, 0] call BIS_fnc_findSafePos;
	
	convoy1 = createGroup EAST;
	[_newPos, 10, "O_APC_Wheeled_02_rcws_F", convoy1] call BIS_fnc_spawnvehicle;
	nul = [convoy1,getMarkerPos _markerAO, 400] call BIS_fnc_taskPatrol;
	sleep 15;
	[_newPos, 10, "O_Truck_03_fuel_F", convoy1] call BIS_fnc_spawnvehicle;
	sleep 15;
	[_newPos, 10, "O_Truck_03_ammo_F", convoy1] call BIS_fnc_spawnvehicle;
	sleep 15;
	[_newPos, 10, "O_Truck_03_covered_F", convoy1] call BIS_fnc_spawnvehicle;
	sleep 15;
	[_newPos, 10, "O_APC_Tracked_02_AA_F", convoy1] call BIS_fnc_spawnvehicle;

	waitUntil {{ alive _x } count units convoy1 == 0}; 

	_null = ["mob_convoy", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerAO;
	deleteGroup convoy1;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_convoy"] call LARs_fnc_removeTask;

	sleep 300;
};

// MAIN LOGIC

_missionDetails = switch (_missionType) do {
	case "arty": {call fn_spawnArtyMission;};
	case "radar": {call fn_spawnRadarMission;};
	case "depot": {call fn_spawnDepotMission;};
	case "convoy": {call fn_spawnConvoyMission;};
};	

nul = [] execVM "scripts\missionAir.sqf";