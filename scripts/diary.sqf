/*
|
| Usage: 
|	
|	Search below for the diary entries you would like to edit. 
|	DiarySubjects appear in descending order when player map is open.
|	DiaryRecords appear in ascending order when selected.
|	Please be respectful and do not remove credit.
|______________________________________________________________*/

if (!hasInterface) exitWith {};

waitUntil {!isNull player};

player createDiarySubject ["Operation", "Operation"];

//-------------------------------------------------- Rules

player createDiaryRecord ["Operation",
[
"Mission",
"
<br /> 1st Platoon clears OBJ DIAMOND and OBJ RUBY to deny enemy use of key facilities north of Kavala.  On order, 1st PLT will conduct reconnaissance of OBJ SAPHIRE to identify enemy positions and movements in the city. 
"
]];

player createDiaryRecord ["Operation",
[
"Enemy Forces",
"
<br />The treat forces in the area are well established and have established a fortified garrison in the city of Kavala.  Reporting on the exact size of enemy forces in the area is limited, but it is clear that the Lords of the Revolution para-military organization has a heavy presence in the area and have been reinforced by an unknown sized element of conventional military forces.  Enemy forces typically operate in squad-sized elements, but are capable of securing key facilities and areas with 2 or more squads.  It is assessed that there is upward of a company sized element operating inside Kavala and Aggelochori (OBJ SAPHIRE), with an additional platoon sized element securing key areas to the north of the city.  Additionally, reporting indicates that there may be a platoon-sized element of armored personnel carriers, as well as 2-3 sections of air defense teams. Further, it is assessed that at least a platoon-sized element of conventional forces are augmenting the garrison, though it is unclear who they are.
"
]];


player createDiaryRecord ["Operation",
[
"Friendly Forces",
"
<br />Adjacent Units
<br />     - Bravo Company, 1st Battalion, 23rd GD is conducting operations south west at Agios Konstanitinos to defend the port facility.
<br />     - Delta Company, 1st Battalion, 23rd GD is defending at Syrta and Kore and screening MSRs.
<br />     - Charlie Company, 1st Battalion, 23rd GD is conducting operations IVO Agios Dionysios
<br />     - 2st Platoon, Alpha Company is defending Abdera.
<br />     - 3rd Platoon, Alpha Company is defending Ammolafi Bay
<br />     - Attachments – 1 x CH-47 from 2-14 CAV SQDN
<br />     - Detachments – There are no detachments for this operation.
"
]];

player createDiaryRecord ["Operation",
[
"SIGNAL/COM",
"
<br />Command
<br />During Phase 1, PLT leadership will be located at the airfield.  During phase 2-3, the PLT leadership will be co-located with 1st SQD during their movement.  During phase 4 PLT leadership will be located at the patrol base.  During phase 5, the PLT leadership will be co-located with 1st SQD during their movement. 
<br />Succession of Command is PL, PSG, 1SL, 2SL, 3SL, Team leaders, then by rank
<br />No adjustments to the SOP
<br />Signal
<br />SOI
<br />     1st Platoon – 50MHz – SPARTAN 1-6
<br />     1st SQD – 50.1MHz – SPARTAN 1-1  
<br />     2nd SQD – 50.2MHz – SPARTAN 1-2
<br />     3rd SQD – 50.3MHz – SPARTAN 1-3
<br />     4th SQD – 50.4MHz – SPARTAN 1-4
<br />     Fires Net – 200.0MHz – OVERLORD STEEL
<br />     Emergency Net – 201.0MHz – EAGLE EYE
<br />Communications Priority – Radio, Verbal, Visual Signals, Runner
<br />Visual Signals
<br />     Green Flare – Shift Fire
<br />     Red Flare – Cease Fire
<br />     Blue Smoke – Clear LZ
<br />     Red Smoke – Hot LZ
<br />     Green Chemlight – Building Clear
"
]];
