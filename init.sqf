﻿enableSaving [false,false];
enableTeamswitch false;


// TcB AIS Wounding System 
if (!isDedicated) then {
	TCB_AIS_PATH = "scripts\ais_injury\";
	{[_x] call compile preprocessFile (TCB_AIS_PATH+"init_ais.sqf")} forEach (if (isMultiplayer) then {playableUnits} else {switchableUnits});
};

//Server Initialization Scripts ===================================================
[] spawn {call compile preprocessFileLineNumbers "scripts\EPD\Ied_Init.sqf";};
_igiload = execVM "scripts\IgiLoad\IgiLoadInit.sqf";

